# Makefile for building the RPi-LCD library
#
# Made by Bram Wesselink https://gitlab.com/users/UseAfterFreee

# Important folders
BIN_DIR=./bin
OBJ_DIR=./obj
SRC_DIR=./src

# First we need to figure out what type of compilation we want
# - debug
# - release

ifndef BUILD_TYPE
# Phony targets
.PHONY: debug release

debug: export BUILD_TYPE :=debug
debug: export BUILD_CFLAGS :=-O0 -g -Wall
debug: export BUILD_LDFLAGS :=-lwiringPi
release: export BUILD_TYPE :=release
release: export BUILD_CFLAGS :=
release: export BUILD_LDFLAGS :=-lwiringPi

debug release:
	@$(MAKE)

else
# Compiler
CC=g++

CFLAGS=$(BUILD_CFLAGS)
LDFLAGS=$(BUILD_LDFLAGS)


# Sources and headers
# Note headers can both and in .hpp and .h
SRC_FILES=$(wildcard $(SRC_DIR)/*.cpp)
HDR_FILES=$(wildcard $(SRC_DIR)/*.hpp) $(wildcard $(SRC_DIR)/*.h)

# Object files that will be generated
OBJ_FILES=$(patsubst $(SRC_DIR)/%.cpp,$(OBJ_DIR)/%.o,$(SRC_FILES))

# Executables that will be generated
EXE=$(BIN_DIR)/$(BUILD_TYPE)

$(EXE): $(OBJ_FILES)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp $(HDR_FILES)
	$(CC) -c $(CFLAGS) -o $@ $<

endif  # BUILD_TYPE

.PHONY: clean

clean:
	rm $(BIN_DIR)/* $(OBJ_DIR)/*
