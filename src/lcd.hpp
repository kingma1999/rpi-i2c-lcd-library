#ifndef LCD_HPP_
#define LCD_HPP_

#include <wiringPiI2C.h>
#include <wiringPi.h>

#define I2C_ADDR   0x27 // I2C device address
#define LINE1  0x80 // 1st line
#define LINE2  0xC0 // 2nd line
#define LINE3  0x94 // 3th line
#define LINE4  0xD4 // 4th line

// commands
#define LCD_CLEARDISPLAY 0x01
#define LCD_RETURNHOME 0x02
#define LCD_ENTRYMODESET 0x04
#define LCD_DISPLAYCONTROL 0x08
#define LCD_CURSORSHIFT 0x10
#define LCD_FUNCTIONSET 0x20
#define LCD_SETCGRAMADDR 0x40
#define LCD_SETDDRAMADDR 0x80

// flags for display entry mode
#define LCD_ENTRYRIGHT 0x00
#define LCD_ENTRYLEFT 0x02
#define LCD_ENTRYSHIFTINCREMENT 0x01
#define LCD_ENTRYSHIFTDECREMENT 0x00

// flags for display on/off control
#define LCD_DISPLAYON 0x04
#define LCD_DISPLAYOFF 0x00
#define LCD_CURSORON 0x02
#define LCD_CURSOROFF 0x00
#define LCD_BLINKON 0x01
#define LCD_BLINKOFF 0x00

// flags for display/cursor shift
#define LCD_DISPLAYMOVE 0x08
#define LCD_CURSORMOVE 0x00
#define LCD_MOVERIGHT 0x04
#define LCD_MOVELEFT 0x00

// flags for function set
#define LCD_8BITMODE 0x10
#define LCD_4BITMODE 0x00
#define LCD_2LINE 0x08
#define LCD_1LINE 0x00
#define LCD_5x10DOTS 0x04
#define LCD_5x8DOTS 0x00

// flags for backlight control
#define LCD_BACKLIGHT 0x08
#define LCD_NOBACKLIGHT 0x00

class LCD {
    private:
        void lcd_byte(int bits, int mode);
        void lcd_toggle_enable(int bits);
    public:
        void Home(void);
        void printI(int i);
        void printF(float myFloat);
        void setCursor(int line, int col); //move cursor
        void Clear(void); // clr LCD return home
        void printS(const char *s);
        void printC(char val);
        void SetCustomChars(void);
        void toggleUnderCursor(int State = -1);
        void toggleBlinkCursor(int State = -1);

        int fd;

        LCD();
};

#endif  // LCD_HPP_
